package hello.validators;

import hello.models.dao.UserDao;
import hello.validators.annotations.Unique;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class UserValidator implements ConstraintValidator<Unique, String> {

    @Autowired
    UserDao userDao;

    private Unique.UniqueField field;

    @Override
    public void initialize(Unique annotation) {
        field = annotation.value();
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        switch (field) {
            case EMAIL:
                if (!userDao.checkUniqueByMail(value))
                    return false;
                break;
            case USERNAME:
                if (!userDao.checkUniqueByUsername(value))
                    return false;
                break;
        }
        return true;
    }

}

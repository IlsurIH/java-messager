package hello.validators.annotations;

import hello.validators.UserValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = UserValidator.class)
public @interface Unique {

    UniqueField value();

    String message() default "{ilsurih.validation.constraints.Unique.message}";

    String field() default "";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    enum UniqueField {
        EMAIL,
        USERNAME

    }
}
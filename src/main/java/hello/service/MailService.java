package hello.service;

import hello.models.Mail;
import hello.models.user.User;
import hello.models.dao.MailDao;
import hello.models.dao.UserDao;
import hello.models.dto.MailDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class MailService {

    final Logger log = Logger.getLogger("MailService");

    @Autowired
    MailDao mailDao;

    @Autowired
    UserDao userDao;

    public List<MailDTO> getMails(String sortField, Boolean asc) {
        log.info("Sorting mail list by " + sortField + " " + asc);
        if (asc == null) asc = false;
        if (sortField == null) sortField = "sendDate";
        List<MailDTO> mails;
        User currentUser = userDao.currentUser();
        if (currentUser.isAdmin()) {
            mails = mailDao.getMails(sortField, asc).stream().map(this::mapAdmin).collect(Collectors.toList());
        } else {
            mails = mailDao.getUserMails(sortField, asc, currentUser.getId()).stream().map(this::map).collect(Collectors.toList());
        }

        return mails;
    }

    public void sendMail(MailDTO mailDTO, int toId) {
        log.info("Sendind mail to " + toId);
        Mail mail = new Mail();
        mail.setSubject(mailDTO.getSubject());
        mail.setText(mailDTO.getText());
        mail.setFrom(userDao.currentUser());
        mail.setTo(userDao.findOne(toId));
        mail.setSendDate(new Date());
        mailDao.save(mail);
    }

    public MailDTO getMail(int id) {
        log.info("Getting mail by id: " + id);
        Mail mail = mailDao.findOne(id);
        User user = userDao.currentUser();
        if (user.isAdmin() || mail.getFrom().equals(user) || mail.getTo().equals(user)) {
            MailDTO map = map(mail);
            map.setText(mail.getText());
            return map;
        }
        else throw new SecurityException("Access denied!");
    }

    public void delete(int id) {
        log.info("Deleting mail by id: " + id);
        Mail one = mailDao.findOne(id);
        User u = userDao.currentUser();
        if (u.isAdmin() || one.getTo().equals(u) || one.getFrom().equals(u)) {
            mailDao.delete(one);
        }
        else throw new SecurityException("Access denied!");
    }

    private MailDTO map(Mail mail) {
        return new MailDTO(mail);
    }

    private MailDTO mapAdmin(Mail mail) {
        MailDTO mailDTO = new MailDTO(mail);
        mailDTO.setTo(mail.getTo().getPersonal());
        return mailDTO;
    }
}

package hello.service;

import hello.models.dao.UserDao;
import hello.models.dto.UserDTO;
import hello.models.user.User;
import hello.models.user.UserCreateDTO;
import hello.models.user.UserUpdateDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindException;

import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class UserService {

    final Logger log = Logger.getLogger("UserService");

    @Autowired
    UserDao userDao;

    public void save(UserCreateDTO userDto) {
        log.info("Saving new user");
        User currentUser = userDao.currentUser();
        User user = map(userDto);
        if (currentUser == null || !currentUser.isAdmin()) {
            //if this called from registration or by non-admin user
            user.setAdmin(false);
        }
        userDao.save(user);
    }

    public void remove(int userId) {
        log.info("Saving user by id: " + userId);
        userDao.delete(userId);
    }

    public void update(UserUpdateDTO user) throws BindException {
        log.info("Updating user");
        validateUser(user);
        userDao.save(map(user));
    }

    public List<UserDTO> getAll(String search, String column, boolean asc) {
        log.info("Receiving users list");
        return userDao.getUsers(search, column, asc)
                .stream()
                .map(UserDTO::new)
                .collect(Collectors.toList());
    }

    public List<UserDTO> getAll(String search) {
        return getAll(search, "username", false);
    }

    public User getById(int id) {
        return userDao.findOne(id);
    }

    public boolean isAdmin() {
        return userDao.currentUser().isAdmin();
    }

    public void updatePassword(String newPassword) {
        User user = userDao.currentUser();
        user.setPassword(newPassword);
        userDao.save(user);
    }

    private User map(UserUpdateDTO dto) {
        User user = new User();
        user.setId(dto.getId());
        user.setEmail(dto.getEmail());
        user.setAdmin(dto.isAdmin());
        user.setPassword(dto.getPassword());
        user.setUsername(dto.getUsername());
        user.setName(dto.getName());
        user.setSurname(dto.getSurname());
        return user;
    }

    private void validateUser(UserUpdateDTO user) throws BindException {
        log.info("Validating user");
        BindException bindException = null;
        User currentUser = userDao.findOne(user.getId());
        if (!currentUser.getUsername().equals(user.getUsername())) {
            if (!userDao.checkUniqueByUsername(user.getUsername())) {
                bindException = new BindException(new BindException(user, "user"));
                bindException.rejectValue("username", "code", "Choose another username");
            }
        }
        if (!currentUser.getEmail().equals(user.getEmail())) {
            if (!userDao.checkUniqueByMail(user.getEmail())) {
                if (bindException == null)
                    bindException = new BindException(new BindException(user, "user"));
                bindException.rejectValue("username", "code", "Choose another email");
            }
        }
        if (bindException != null)
            throw bindException;
    }
}

package hello.service;

import hello.models.Relation;
import hello.models.user.User;
import hello.models.dao.RelationDao;
import hello.models.dao.UserDao;
import hello.models.dto.ContactDTO;
import hello.models.dto.UserDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Service
public class ContactService {

    final Logger log = Logger.getLogger("ContactService");

    @Autowired
    UserDao userDao;

    @Autowired
    UserService userService;

    @Autowired
    RelationDao relationDao;

    public List<UserDTO> getUserContacts(String query, boolean global) {
        log.info("Getting contacts by query: '" + query + "'");
        List<Relation> contacts = userDao.getUsersContacts(query, userDao.currentUser().getId());
        if (contacts == null) return null;
        List<UserDTO> dtos = contacts
                .stream().map(r -> new ContactDTO(r.getSlave(), r.getId())).collect(Collectors.toList());
        if (global)
            userService.getAll(query).forEach(u -> {
                if (!dtos.contains(u)) {
                    dtos.add(u);
                }
            });
        return dtos;
    }

    public void addContact(int id) {
        log.info("Adding contact: '" + id + "'");
        User user = userDao.currentUser();
        List<User> contacts = user.getContacts();
        if (contacts == null) {
            contacts = new ArrayList<>(1);
            user.setContacts(contacts);
        }
        contacts.add(userDao.findOne(id));
        userDao.save(user);
    }

    public void removeContact(int id) {
        log.info("Removing contact: '" + id + "'");
        Relation relation = relationDao.findOne(id);
        Integer userId = userDao.currentUser().getId();
        if (relation.getMaster().getId().equals(userId))
            relationDao.delete(relation);
        else throw new SecurityException("Access denied!");
    }
}

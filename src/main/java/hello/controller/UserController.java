package hello.controller;

import hello.models.dto.UserDTO;
import hello.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public List<UserDTO> users(
            @RequestParam(defaultValue = "") String search,
            @RequestParam(defaultValue = "username") String column,
            @RequestParam(defaultValue = "false") Boolean asc) {
        return userService.getAll(search, column, asc);
    }

    @RequestMapping(value = "isadmin", method = RequestMethod.GET)
    public boolean isadmin() {
        return userService.isAdmin();
    }

    @RequestMapping(value = "password", method = RequestMethod.PUT)
    public ResponseEntity<Exception> updatePassword(String newPassword1, String newPassword2) throws Exception {
        Exception exception = checkPassword(newPassword1, newPassword2);
        if (exception != null) {
            exception.setStackTrace(new StackTraceElement[0]);
            return new ResponseEntity<>(exception, HttpStatus.BAD_REQUEST);}
        userService.updatePassword(newPassword1);
        return null;
    }

    private Exception checkPassword(String newPassword1, String newPassword2) throws Exception {
        if (newPassword1 == null) return new Exception("Password cannot be null");
        if (newPassword1.length() < 3) return new Exception("Password's length must me more than 3");
        if (newPassword1.length() > 31) return new Exception("Password's length must me less than 32");
        if (!newPassword1.equals(newPassword2)) return new Exception("Passwords does not match");
        return null;
    }
}

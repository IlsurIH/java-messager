package hello.controller;

import hello.models.dto.MailDTO;
import hello.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("mails")
public class MailController {

    @Autowired
    MailService mailService;

    @RequestMapping(method = RequestMethod.GET)
    public List<MailDTO> mails(
            @RequestParam(defaultValue = "sendDate") String column,
            @RequestParam(defaultValue = "false") Boolean asc
    ) {
        return mailService.getMails(column, asc);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public MailDTO mail(@PathVariable int id) {
        return mailService.getMail(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id) {
        mailService.delete(id);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void send(MailDTO mail, @RequestParam("to_id") int toId) {
        mailService.sendMail(mail, toId);
    }
}

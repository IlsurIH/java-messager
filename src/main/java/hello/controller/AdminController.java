package hello.controller;

import hello.models.user.User;
import hello.models.user.UserUpdateDTO;
import hello.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("users")
@Secured("ROLE_ADMIN")
public class AdminController {

    @Autowired
    UserService userService;

    @RequestMapping(value = "{id}", method = RequestMethod.GET)
    public User user(@PathVariable int id) {
        return userService.getById(id);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void remove(@PathVariable int id) {
        userService.remove(id);
    }

    @RequestMapping(method = RequestMethod.PUT)
    public void update(@Valid UserUpdateDTO user) throws BindException {
        userService.update(user);
    }
}

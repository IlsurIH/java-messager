package hello.models.dao;

import hello.models.Relation;
import hello.models.user.User;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.List;

public interface UserDao extends GenericDao<User> {

    User findByUsername(String username) throws UsernameNotFoundException;

    User currentUser();

    List<User> getUsers(String search, String sortField, boolean asc);

    List<Relation> getUsersContacts(String search, int userId);

    boolean checkUniqueByMail(String email);

    boolean checkUniqueByUsername(String username);
}

package hello.models.dao;

import hello.models.Mail;

import java.util.List;

public interface MailDao extends GenericDao<Mail> {
    List<Mail> getMails(String sortField, boolean asc);

    List<Mail> getUserMails(String sortField, boolean asc, int userId);
}

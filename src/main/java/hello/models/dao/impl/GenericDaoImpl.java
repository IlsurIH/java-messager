package hello.models.dao.impl;

import hello.models.MainEntity;
import hello.models.dao.GenericDao;
import org.hibernate.Session;
import org.springframework.data.jpa.repository.support.JpaEntityInformation;
import org.springframework.data.jpa.repository.support.JpaEntityInformationSupport;
import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import javax.persistence.EntityManager;

public abstract class GenericDaoImpl<T extends MainEntity> extends SimpleJpaRepository<T, Integer> implements GenericDao<T> {

    protected final EntityManager em;

    private final JpaEntityInformation<T, ?> entityInformation;

    public GenericDaoImpl(Class<T> domainClass, EntityManager em) {
        super(domainClass, em);
        this.em = em;
        entityInformation = JpaEntityInformationSupport.getEntityInformation(domainClass, em);
    }

    @Override
    public <S extends T> S save(S entity) {
        if (entityInformation.isNew(entity)) {
            em.persist(entity);
            return entity;
        } else {
            return em.merge(entity);
        }
    }

    protected Session getCurrentSession() {
        return em.unwrap(Session.class);
    }
}

package hello.models.dao.impl;

import hello.models.Relation;
import hello.models.dao.UserDao;
import hello.models.user.User;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends GenericDaoImpl<User> implements UserDao {

    public UserDaoImpl(EntityManager em) {
        super(User.class, em);
    }

    @Override
    public User findByUsername(String username) throws UsernameNotFoundException {
        Object result = getCurrentSession()
                .createCriteria(User.class)
                .add(Restrictions.eq("username", username))
                .uniqueResult();
        if (result == null) throw new UsernameNotFoundException(username);
        return (User) result;
    }

    @Override
    public User currentUser() {
        Object details = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (details instanceof User) {
            User user = (User) details;
            user = findOne(user.getId());
            return user;
        }
        return null;
    }

    @Override
    public List<User> getUsers(String search, String sortField, boolean asc) {
        if (sortField.equals("admin")) sortField = "isAdmin";
        Criteria criteria = getCurrentSession().createCriteria(User.class)
                .addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
        return addSearchCriterion(search, criteria).list();
    }

    private Criteria addSearchCriterion(String search, Criteria criteria1) {
        if (!"".equals(search)) {
            search = "%" + search + "%";
            criteria1.add(Restrictions.or(
                    Restrictions.like("username", search),
                    Restrictions.like("name", search),
                    Restrictions.like("surname", search),
                    Restrictions.like("email", search)
            ));
        }
        return criteria1;
    }

    @Override
    public List<Relation> getUsersContacts(String search, int userId) {

        Criteria criteria = getCurrentSession()
                .createCriteria(Relation.class)
                .add(Restrictions.eq("master.id", userId));

        return addSearchCriterion(search, criteria
                .createCriteria("slave"))
                .list();
    }

    @Override
    public boolean checkUniqueByMail(String email) {
        Long count = (Long) getCurrentSession().createQuery("select count (u) from User u where email = :field")
                .setParameter("field", email)
                .uniqueResult();
        return count == 0;
    }

    @Override
    public boolean checkUniqueByUsername(String username) {
        Long count = (Long) getCurrentSession().createQuery("select count (u) from User u where username = :field")
                .setParameter("field", username)
                .uniqueResult();
        return count == 0;
    }
}

package hello.models.dao.impl;

import hello.models.Mail;
import hello.models.dao.MailDao;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import java.util.List;

@Repository
public class MailDaoImpl extends GenericDaoImpl<Mail> implements MailDao {

    public MailDaoImpl(EntityManager em) {
        super(Mail.class, em);
    }

    @Override
    public List<Mail> getMails(String sortField, boolean asc) {
        return (List<Mail>) getSortedMailCriteria(sortField, asc).list();
    }

    private Criteria getSortedMailCriteria(String sortField, boolean asc) {
        return getCurrentSession().createCriteria(Mail.class)
                .addOrder(asc ? Order.asc(sortField) : Order.desc(sortField));
    }

    @Override
    public List<Mail> getUserMails(String sortField, boolean asc, int userId) {
        return getSortedMailCriteria(sortField, asc).add(Restrictions.or(
                Restrictions.eq("to.id", userId), Restrictions.eq("from.id", userId)
        )).list();
    }
}

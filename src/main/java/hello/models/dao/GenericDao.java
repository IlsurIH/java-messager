package hello.models.dao;

import hello.models.MainEntity;
import org.springframework.data.repository.CrudRepository;

public interface GenericDao<T extends MainEntity> extends CrudRepository<T, Integer> {
}

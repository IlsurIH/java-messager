package hello.models;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
public class MainEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object obj) {
        if (getId() == null) return false;
        if (obj.getClass().equals(this.getClass()))
            return this.getId().equals(((MainEntity) obj).getId());
        return super.equals(obj);
    }
}

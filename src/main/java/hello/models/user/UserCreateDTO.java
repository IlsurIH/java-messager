package hello.models.user;

import hello.validators.annotations.Unique;

public class UserCreateDTO extends UserUpdateDTO {

    @Override
    @Unique(message = "Choose another username", value = Unique.UniqueField.USERNAME)
    public String getUsername() {
        return super.getUsername();
    }

    @Override
    @Unique(message = "Choose another email", value = Unique.UniqueField.EMAIL)
    public String getEmail() {
        return super.getEmail();
    }
}

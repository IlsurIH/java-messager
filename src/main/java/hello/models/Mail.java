package hello.models;

import hello.models.user.User;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "mails")
public class Mail extends MainEntity {
    @ManyToOne
    @JoinColumn(name = "from_id")
    User from;

    @ManyToOne
    @JoinColumn(name = "to_id")
    User to;

    @Basic
    @Column
    String subject;

    @Basic
    @Column
    String text;

    @Basic
    @Column(name = "send_date")
    Date sendDate;

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public User getTo() {
        return to;
    }

    public void setTo(User to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getSendDate() {
        return sendDate;
    }

    public void setSendDate(Date sendDate) {
        this.sendDate = sendDate;
    }
}

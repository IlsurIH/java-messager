package hello.models;

import hello.models.user.User;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "relations")
public class Relation extends MainEntity{

    @ManyToOne
    @JoinColumn(name = "master_id")
    User master;

    @ManyToOne
    @JoinColumn(name = "slave_id")
    User slave;

    public User getMaster() {
        return master;
    }

    public void setMaster(User master) {
        this.master = master;
    }

    public User getSlave() {
        return slave;
    }

    public void setSlave(User slave) {
        this.slave = slave;
    }
}

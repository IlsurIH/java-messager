package hello.models.dto;

import hello.models.user.User;

public class ContactDTO extends UserDTO {

    int relation;

    public ContactDTO(User user) {
        super(user);
    }

    public ContactDTO(User slave, Integer id) {
        this(slave);
        setRelation(id);
    }

    public int getRelation() {
        return relation;
    }

    public void setRelation(int relation) {
        this.relation = relation;
    }
}

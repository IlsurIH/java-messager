package hello.models.dto;

import hello.models.Mail;

public class MailDTO {
    private int id;
    private String from;
    private String to;
    private String subject;
    private String sendDate;
    private String text;

    public MailDTO() {
    }

    public MailDTO(Mail mail) {
        setId(mail.getId());
        setSendDate(new MyDate(mail.getSendDate().getTime()).toString());
        setFrom(mail.getFrom().getPersonal());
        setSubject(mail.getSubject());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}

package hello.models.dto;

import java.text.DateFormat;
import java.text.Format;
import java.util.Calendar;
import java.util.Date;

public class MyDate extends Date {

    private static final Date yesterday = getYesterday();

    private static final Format hours = DateFormat.getTimeInstance();
    private static final Format days = DateFormat.getDateTimeInstance();

    public MyDate() {
    }

    public MyDate(long date) {
        super(date);
    }

    @Override
    public String toString() {
        if (after(yesterday)) {
            return hours.format(this);
        } else return days.format(this);
    }

    private static Date getYesterday() {
        Calendar instance = Calendar.getInstance();
        instance.set(Calendar.DAY_OF_MONTH, instance.get(Calendar.DAY_OF_MONTH) - 1);
        instance.set(Calendar.HOUR_OF_DAY, 0);
        instance.set(Calendar.MINUTE, 0);
        return instance.getTime();
    }
}

package hello.models.dto;

import hello.models.user.User;

public class UserDTO {

    int id;

    String username;

    String email;

    String name;

    String surname;

    boolean isAdmin;

    public UserDTO(User user) {
        setId(user.getId());
        setUsername(user.getUsername());
        setName(user.getName());
        setSurname(user.getSurname());
        setEmail(user.getEmail());
        setAdmin(user.isAdmin());
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }


    @Override
    public boolean equals(Object obj) {
        if (obj instanceof UserDTO) {
            return id == ((UserDTO) obj).getId();
        }
        return super.equals(obj);
    }
}

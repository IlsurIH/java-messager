loadUsers = function (column, asc) {
    if (column == null) column = mailListRactive.get('sortColumn');
    if (asc == null) asc = mailListRactive.get('asc');
    $.ajax({
        url: 'users',
        data: 'column=' + column + '&asc=' + asc,
        success: function (data) {
            mailListRactive.set('data', data);
        }
    });
};

userEditRactiveSaveSuccess = function (data) {
    loadUsers();
};

mailListRactive = new Ractive({
    el: '#userContainer',
    template: '#userListTemplate',
    data: {
        sortColumn: 'username',
        asc: false
    }
});
mailListRactive.on('sort', function (event, column) {
    var asc = !event.node.classList.contains('asc');
    $(".sortable").each(function () {
        $(this).removeClass("asc desc");
    });
    event.node.classList.add(asc ? 'asc' : 'desc');
    this.set('sortColumn', column);
    this.set('asc', asc);
    loadUsers(column, asc);
});
mailListRactive.on('edit', function (event, id) {
    $.ajax({
        url: 'users/' + id,
        success: function (data) {
            userEditRactive.edit(data)
        }
    });
});
mailListRactive.on('remove', function (event, id) {
    if (confirm('Do you really want to remove this user?')) {
        $.ajax({
            url: 'users/' + id,
            type: 'DELETE',
            success: function (data) {
                loadUsers();
            }
        });
    }
});
$("#createUser").click(function () {
    userEditRactive.edit()
});
loadUsers();

$.ajax({
    url: 'users/isadmin',
    success: function (data) {
        if (data == true)
            userEditRactive.set('is_admin', true)
    }
});
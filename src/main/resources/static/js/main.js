loadMails = function (column, asc) {
    $.ajax({
        url: 'mails',
        data: 'column=' + column + '&asc=' + asc,
        success: function (data) {
            mailListRactive.set('mails', data);
        }
    });
};

mailEditModal = new Ractive({
    el: '#modalContainer',
    template: '#messagetemplate',
    showMail: function (mail) {
        this.set(mail).then(function () {
            $('#messageModal').modal('show');
        })
    },
    show: function (to_id, name) {
        this.set('to_id', to_id);
        this.set('to', name);
        this.set('id', null);
        this.set('subject', null);
        this.set('text', null);
        $('#messageModal').modal('show');
    }
});
mailEditModal.on('send', function(){
    $.post(
        'mails',
        $('#mailEditForm').serialize(),
        function (data) {
            $('#messageModal').modal('hide');
            loadMails(mailListRactive.get('sortColumn'), mailListRactive.get('asc'));
        })
        .fail(function (xhr, status, error) {
            console.log(error);
        });
});
mailEditModal.on('delete', function(event, id) {
    $.ajax({
        url: 'mails/' + id,
        type: 'DELETE',
        success: function (data) {
            $('#messageModal').modal('hide');
            loadMails(mailListRactive.get('sortColumn'), mailListRactive.get('asc'));
        },
        fail: function (xhr, status, error) {
            console.log(error);
        }
    });
});
mailListRactive = new Ractive({
    el: '#mailContainer',
    template: '#mailListTemplate',
    data: {
        sortColumn: 'sendDate',
        asc: false
    }
});
$.ajax({
    url: 'users/isadmin',
    success: function (data) {
        mailListRactive.set('isadmin', data);
        if (!data)
            $('#adminButton').remove()
    }
});
mailListRactive.on('show', function (event, id) {
    $.ajax({
        url: 'mails/' + id,
        success: function (data) {
            mailEditModal.showMail(data)
        }
    });
});
mailListRactive.on('sort', function (event, column) {
    var asc = !event.node.classList.contains('asc');
    $(".sortable").each(function () {
        $(this).removeClass("asc desc");
    });
    event.node.classList.add(asc ? 'asc' : 'desc');
    this.set('sortColumn', column);
    this.set('asc', asc);
    loadMails(column, asc);
});
loadMails(mailListRactive.get('sortColumn'), mailListRactive.get('asc'));

filterContacts = function (query, global) {
    $.ajax({
        url: 'contacts',
        data: 'query=' + query + '&global=' + global,
        success: function (data) {
            contactRactive.set('contacts', data);
        }
    });
};

contactRactive = new Ractive({
    el: '#contactContainer',
    template: '#contacttemplate',
    data: {
        query: '',
        global: false
    },
    onDelete: function (id) {
        $.ajax({
            url: 'contacts/' + id,
            type: 'DELETE',
            success: function () {
                contactRactive.update()
            }
        });
    },
    onAdd: function (id) {
        $.ajax({
            url: 'contacts',
            data: 'id='+ id,
            type: 'POST',
            success: function () {
                contactRactive.update()
            }
        });
    },
    update: function () {
        filterContacts(this.get('query'), this.get('global'))
    }
});
contactRactive.on('send', function (event, id, name) {
    mailEditModal.show(id, name);
});
contactRactive.on('deleteContact', function (event, id) {
    contactRactive.onDelete(id);
});
contactRactive.on('addContact', function (event, id) {
    contactRactive.onAdd(id);
});
contactRactive.observe('query', function(newValue, oldValue, keypath) {
    filterContacts(newValue, this.get('global'))
});
contactRactive.observe('global', function(newValue, oldValue, keypath) {
    filterContacts(this.get('query'), newValue)
});
contactRactive.update();

passwordRactive = new Ractive({
    el: '#passwordContainer',
    template: '#passwordtemplate',
    onSave: function () {
        $.ajax({
            url: 'users/password',
            data: 'newPassword1=' + passwordRactive.get('newPassword1') + '&newPassword2=' + passwordRactive.get('newPassword2'),
            type: 'PUT',
            success: function () {
                $('#passwordModal').modal('hide');
                passwordRactive.set('newPassword1', null);
                passwordRactive.set('newPassword2', null)
            },
            error : function (xhr, status, error)  {
                passwordRactive.set('error', xhr.responseJSON.message)
            }
        });
    }
});
passwordRactive.on('save', function(event) {
    this.onSave()
});

$('#changePassword').click(function () {
    passwordRactive.set('error', null);
    passwordRactive.set('newPassword1', null);
    passwordRactive.set('newPassword2', null);
    $('#passwordModal').modal('show');
});
var userEditRactive;

userEditRactiveSaveSuccess = function (data) {
    console.error('save success should be overridden')
};
userEditRactiveSaveFail = function (error) {
    console.error('save error should be overridden')
};

if ($('modalContainer') == null) console.error('modalContainer could not be found!');

$.get('/ractive/newuser.html').then(function (template) {
    userEditRactive = new Ractive({
        el: '#modalContainer',
        template: template,
        data: {
            is_admin: false
        },
        edit: function (user) {
            if (user == null)
                user = {
                    id: null,
                    username: null,
                    password: null,
                    name: null,
                    surname: null,
                    email: null,
                    admin: null
                };
            clearErrorMsg();
            userEditRactive.set(user).then(function () {
                $('#userModal').modal('show');
            });
        },
        saveSuccess: userEditRactiveSaveSuccess,
        saveFail: userEditRactiveSaveFail
    });
    userEditRactive.on('submit', function (event) {
        clearErrorMsg();
        var requestUrl;
        var requestType;
        if (userEditRactive.get('id') == null) {
            requestUrl = 'register';
            requestType = 'POST';
        } else {
            requestUrl = 'users';
            requestType = 'PUT';
        }
        $.ajax({
            url: requestUrl,
            type: requestType,
            data: $('#userEditForm').serialize(),
            success: function (data) {
                $('#userModal').modal('hide');
                userEditRactive.saveSuccess(data);
            }
        }).fail(function (xhr, status, error) {
            xhr.responseJSON.errors.forEach(function (e) {
                    userEditRactive.set(e.field + '_err', e.defaultMessage)
                }
            );
            userEditRactive.saveFail(error);
        });
    });
    console.log('usereditractive loading complete')
});

function clearErrorMsg() {
    userEditRactive.set('username_err', null);
    userEditRactive.set('name_err', null);
    userEditRactive.set('surname_err', null);
    userEditRactive.set('password_err', null);
    userEditRactive.set('email_err', null);
}

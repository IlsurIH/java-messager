package hello.service;

import hello.models.dao.UserDao;
import hello.models.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.annotations.Test;

import java.util.Iterator;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext
public class UserServiceTest extends AbstractTestNGSpringContextTests {

    @LocalServerPort
    private int port;

    @Autowired
    UserService userService;

    @Autowired
    UserDao userDao;

    @Test
    public void testHome() throws Exception {
        ResponseEntity<String> entity = new TestRestTemplate()
                .getForEntity("http://localhost:" + this.port, String.class);
        assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(entity.getBody()).isEqualTo("Hello World");
    }

    @Test
    public void testSave() {
        User user = new User();
        user.setEmail("hello@mail.ru");
        user.setUsername("user1");
        user.setPassword("user1");
        user.setName("test");
        user.setSurname("user");
//        userService.save(user);
    }

    @Test
    public void testSaveFailure() {
        User user1 = getUser();
        if (user1 == null) return;

        User user = new User();
        user.setEmail("hello1@mail.ru");
        user.setUsername("user12");
        user.setPassword("user1");
        user.setName("test");
        user.setId(user1.getId());
        user.setSurname("user");
//        assertThatThrownBy(() -> userService.save(user))
//                .isInstanceOf(DataIntegrityViolationException.class);
    }

    @Test
    public void testUpdate() {
        User user = getUser();
        if (user == null) return;
        user.setAdmin(!user.isAdmin());
//        userService.save(user);
        assertThat(getUser().isAdmin() == user.isAdmin());
    }
    private User getUser() {
        Iterator<User> iterator = userDao.findAll().iterator();
        if (iterator.hasNext())
            return iterator.next();
        return null;
    }
}
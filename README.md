# Info #

Пример реализации сервиса обмена сообщениями с технологией ajax

### Устройство серверной части ###

* Spring Boot (MVC + Security + Spring Data)
* Hibernate
* PostgreSQL

### Устройство клиентовской части ###

* Ractive JS
* Bootstrap

### Сброрка проекта ###

* Для сборки в качестве executable jar (при запуске будет инициализирован встроенный контейнер сервлетов) запустить комманду 
```
#!maven

mvn -B -DskipTests=true clean install
```
* Для сборки war архива изменить pom.xml, добавив строку 
```
#!xml

<scope>provided</scope>
```
 в зависимость spring-boot-starter-tomcat и изменить значение атрибута 
```
#!xml

<packaging>
```
 на war

### Перед сборкой измените содержимое файла JavaMessager/src/main/resources/application.properties для использования собственной базы данных ###
В файле database.txt написаны запросы для создания таблиц в базе данных
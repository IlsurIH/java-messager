
CREATE TABLE mails
(
  id INTEGER PRIMARY KEY NOT NULL,
  subject VARCHAR(30) NOT NULL,
  text TEXT NOT NULL,
  send_date TIMESTAMP DEFAULT now() NOT NULL,
  from_id INTEGER NOT NULL,
  to_id INTEGER NOT NULL,
  CONSTRAINT fkf2bwqbwobtqsea7i7ttawjtmj FOREIGN KEY (from_id) REFERENCES users (id),
  CONSTRAINT to_id FOREIGN KEY (to_id) REFERENCES users (id),
  CONSTRAINT fkfhscdla0l21ullcxp8u01hsex FOREIGN KEY (to_id) REFERENCES users (id)
);
COMMENT ON COLUMN mails.send_date IS 'timestamp()';
CREATE UNIQUE INDEX "Mails_id_uindex" ON mails (id);
CREATE TABLE relations
(
  id INTEGER PRIMARY KEY NOT NULL,
  master_id INTEGER NOT NULL,
  slave_id INTEGER NOT NULL,
  CONSTRAINT fko4rs5pkl4ulw6n82bc6tffmyu FOREIGN KEY (master_id) REFERENCES users (id),
  CONSTRAINT relations_master_fk FOREIGN KEY (master_id) REFERENCES users (id),
  CONSTRAINT fksuv5084neurgs59jao8kccy0u FOREIGN KEY (slave_id) REFERENCES users (id),
  CONSTRAINT relations_slave_fk FOREIGN KEY (slave_id) REFERENCES users (id)
);
CREATE UNIQUE INDEX relations_id_uindex ON relations (id);
CREATE UNIQUE INDEX relations_unique_index ON relations (master_id, slave_id);
CREATE TABLE users
(
  id INTEGER PRIMARY KEY NOT NULL,
  username VARCHAR(20) NOT NULL,
  name VARCHAR(25) NOT NULL,
  surname VARCHAR(25) NOT NULL,
  password VARCHAR(32) NOT NULL,
  is_admin BOOLEAN,
  email VARCHAR(255) NOT NULL
);
CREATE UNIQUE INDEX "Users_id_uindex" ON users (id);
CREATE UNIQUE INDEX "Users_login_uindex" ON users (username);
CREATE UNIQUE INDEX users_username_uindex ON users (username);
